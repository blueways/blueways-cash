'use strict';

import Bw from '../dist/bw';

$(function () {

    // CASH REBUILD START
    $('html').removeClass('no-js');

    //$.Interchange();
    //$.DesktopNav();

    /*
    $.MobileNav({
        selectorHeader: 'body',
        selectorMenuButton: '.menu-toggle',
        isOpenClass: 'menu-open',
        showFor: ['small', 'medium']
    });
     */


    $.lightbox = new Bw.Lightbox();

    /*
    $.myValidator = $.Validator({
        selector: {
            submitButton: 'form[data-validate="true"] button[type="submit"]',
            closestInputLabelElement: '.form__field'
        }
    });
     */

    $.myWizard = new Bw.Wizardbox({
        steps: [
            {
                id: 0,
                content: '<h1>1. Hello World</h1><a href="#" data-to-step="1">Go to step 2</a>',
                title: 'Welcome',
                size: 'small',
                icon: 'check'
            },
            {
                id: 1,
                content: '<h1>2. Nice Headline</h1><a href="#" data-to-step="2">Go to step 2</a><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet aperiam, aspernatur culpa cupiditate dolore quaerat voluptatem? At, quod, rerum! Consequuntur eius exercitationem facere magnam nisi sed similique sit tempora. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aliquid, aperiam asperiores eius excepturi, harum ipsum iste iusto maiores natus odit placeat possimus sit soluta, veniam? Animi dolor esse vero. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad culpa dolore dolorem dolorum ducimus, excepturi id iure perspiciatis praesentium qui quis quos rem saepe similique tempore! Libero nemo nobis optio!</p>',
                title: 'Headline',
                size: 'medium'
            },
            {
                id: 3,
                content: '<h1>4. Confirmation</h1> <a href="#" data-to-step="success">Submit</a>',
                title: 'Confirm',
                size: 'medium'
            },
            {
                id: 'success',
                content: '<h1>5. Success!</h1> <a href="#" data-close>close</a> <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic impedit, ipsum possimus quasi qui sapiente. Accusantium aliquid architecto blanditiis delectus dolorem dolores, explicabo fugit nam nemo quasi quia sequi velit?</p>',
                size: 'small'
            },
        ]
    });

    // add form to step

    const myForm = $('form');
    $('<h1>2. Form</h1>').prependTo(myForm);
    $(myForm).append('<a href="#" data-to-step="1" data-mark-success="false">back</a>');
    $('.button', myForm).on('click', function (e) {
        e.preventDefault();

        if (!$.myValidator.isValidForm()) {
            $.myWizard.startLoader();

            setTimeout(function () {
                $.myWizard.stopLoader();
                $.myWizard.toStep("3");

            }, 1000);
        }

    });

    $.myWizard.addStep({
        content: myForm,
        id: 2,
        title: 'Select Date'
    }, 2);

    // open wizard at click on any hollow button
    $('.button.hollow').on('click', function (e) {
        e.preventDefault();
        $.myWizard.open(0);
    });

});
