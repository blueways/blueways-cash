$(function () {

    $(document).on('bw.mediaQuery.changed', () => {
        console.log('changed');
    });

    Bw.MediaQuery.defaults = {
        breakpointWidths: [200, 400, 500, 2200, 45000]
    };

    let bw = $(document).bw({
        Lightbox: {
            selector: 'a.lightboxtest'
        },
        MediaQuery: {
            breakpointWidths: [0, 640, 1024, 1200, 1440],
        },
        WizardBox: {
            showNavigator: true,
            steps: [
                {
                    id: 'hello',
                    content: '<h1>Hello World!</h1><a href="#" data-to-step="edit">next</a>',
                    title: 'Hello'
                },
                {
                    id: 'edit',
                    content: '<h1>Edit the World!</h1><a href="#" data-to-step="success">done</a>',
                    title: 'Edit',
                    size: 'medium'
                },
                {
                    id: 'success',
                    content: '<h1>Done</h1><a href="#" data-close>close</a>'
                }
            ]
        }
    });

    console.log(bw);

    let val = new Bw.Validator();
    console.log(val);

    $('#wizardboxLink').on('click', (e) => {
        e.preventDefault();
        bw.WizardBox.open('hello');
    });

    const calendar1 = $('#calendar1').calendar({
        view: 2,
    });

    calendar1.addEvent({
        name: 'Termin 1',
        id: 'x1',
        startDate: new Date(),
        endDate: new Date('2020-07-17')
    }, true);

    calendar1.addEvent({
        name: 'Termin 2',
        id: 'x1',
        startDate: new Date('2020-08-01'),
        endDate: new Date('2020-08-03')
    }, true);

    console.log(calendar1);

    // const calendar2 = $('<div>').calendar({
    //     baseUri: location.origin + '/examples',
    //     calendarUids: [1]
    // }).appendTo($('#calendar2'));







});
