const path = require('path');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');


let devConf = {
    mode: 'development',
    entry: './typescript/bw.ts',
    devServer: {
        contentBase: '.',

    },
    watch: true,
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                            sassOptions: {
                                includePaths: ['node_modules/foundation-sites/scss']
                            }
                        }
                    }
                ]
            }
        ],
    },
    resolve: {
        extensions: ['.js', '.es6', '.ts']
    },
    output: {
        filename: 'bw.combined.js',
        path: path.resolve(__dirname, 'dist'),
        library: 'Bw',
    },
    externals: {
        "cash-dom": "$"
    }
};

const prodConf = {
    mode: 'production',
    output: {
        filename: 'bw.min.js',
        path: path.resolve(__dirname, 'dist'),
        library: 'Bw',
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'bw.min.css',
            chunkFilename: '[id].css',
        })
    ],
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    {
                        loader: 'sass-loader',
                        options: {
                            sassOptions: {
                                includePaths: ['node_modules/foundation-sites/scss'],
                                outputStyle: 'compressed',
                            }
                        }
                    }
                ]
            }
        ],
    },
    entry: devConf.entry,
    resolve: devConf.resolve,
    externals: devConf.externals,
};

module.exports = [devConf, prodConf];
