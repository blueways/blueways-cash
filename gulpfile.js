const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const ts = require('gulp-typescript');
const tsProject = ts.createProject('tsconfig.json');
const webpack = require('webpack-stream');

const sassPaths = [
    'node_modules/foundation-sites/scss'
];

function sass() {
    return gulp.src('src/Scss/**/*.scss')
        .pipe($.sass({
            includePaths: sassPaths,
            outputStyle: 'compressed' // if css compressed **file size**
        })
            .on('error', $.sass.logError))
        .pipe(gulp.dest('src/Css'));
}

function script() {
    return tsProject.src()
        .pipe(tsProject())
        .js
        .pipe(gulp.dest('src/js'));
}

function watch() {
    gulp.watch("src/Scss/**/*.scss", sass);
    gulp.watch("src/typescript/**/*.ts", gulp.series(script, build));
}

function build() {
    return gulp.src('src/example/index.js')
        .pipe(webpack(require('./webpack.config.js')))
        .pipe(gulp.dest('dist/'));
}

function copy() {
}

gulp.task('sass', gulp.series(sass));
gulp.task('watch', gulp.series(sass, watch));
gulp.task('build', gulp.series(sass, script, build, copy));
gulp.task('default', gulp.series(sass, script));
