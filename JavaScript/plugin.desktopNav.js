function DesktopNav(options) {

  // extend default options
  this.settings = $.extend({}, this._defaults, options);

  this.init();

  return this;
}

DesktopNav.prototype = {

  _defaults: {
    $body: null,
    $menuLevel2: null,
  },

  pageY: 0,

  init: function () {
    this._cacheDom();
    this._bindEvents();
  },

  _cacheDom: function () {
    this.$body = $('body');
    this.$menuLevel2 = $('ul.menu.level2');
  },

  _bindEvents: function () {

    $(window).on('scroll', _.throttle(this._onPageScroll.bind(this), 100));

    this.$menuLevel2.each(function () {
      const submenuDirection = $(this).find('.level3.submenu.opens-left').length ? 'left' : 'right';

      // init menuAim
      $(this).menuAim({
        rowSelector: 'ul.level2 > li',
        submenuSelector: '.is-dropdown-submenu-parent',
        submenuDirection: submenuDirection,
        activate: function (row) {
          $(row).find(".submenu").addClass('hover');
        },
        deactivate: function (row) {
          $(row).find(".hover").removeClass('hover');
        },
        enter: function (row) {
          $('a', row).first().addClass('active');
        },
        exit: function (row) {
          $('a', row).first().removeClass('active');
        },
      }).addClass('no-css-hover');
    });

  },

  _onPageScroll: function (win) {
    if (window.scrollY > this.pageY && this.pageY > 1) {
      this.$body.addClass('on-scroll');
    } else {
      this.$body.removeClass('on-scroll');
    }
    this.pageY = window.scrollY;
  }

};

$.DesktopNav = function (options) {
  return new DesktopNav(options);
};
