const Interchange = function (options) {

  // extend default options
  this.settings = $.extend({}, this._defaults, options);

  this.init();

  return this;
};

Interchange.prototype = {

  _defaults: {
    'selector': '[data-interchange]'
  },

  init: function () {
    this._cacheDom();
    this._interchange();
    this._bindListener();
  },

  _cacheDom: function () {
    this.$elements = $(this.settings.selector);
  },

  _interchange: function () {

    const self = this;

    let newFileName = '';

    this.$elements.each(function (i, e) {

      const attr = $(e).attr('data-interchange').match(/(?!\[)([.\S]+\w)/g);

      for (let i = 0; i < attr.length / 2; i++) {
        const size = attr[(i * 2) + 1];

        if ($.MediaQuery.atLeast(size)) {
          newFileName = attr[i * 2]
        }
      }

      // set new file
      if ($(e).prop('tagName') === 'IMG') {
        $(e).attr('src', newFileName);
      } else {
        $(e).css('background-image', 'url(' + newFileName + ')');
      }

    });
  },

  _bindListener: function () {
    $(document).on('changed.bw.mediaquery', this._interchange.bind(this));
  }
};

$.Interchange = function (options) {
  return new Interchange(options)
};
