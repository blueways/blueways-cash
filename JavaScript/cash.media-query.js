/**
 * Start the MediaQuery plugin
 *
 * @param options
 * @returns {MediaQuery}
 * @constructor
 */
function MediaQuery(options) {

  // extend default options
  this.settings = $.extend({}, this._defaults, options);

  this.init();

  return this;
}

MediaQuery.prototype = {


  init: function () {

    this._updateCurrentState();
    this._watcher();

  },

  /**
   * Watch for breakpoint changes, fire event and update currentSize
   * @private
   */
  _watcher: function () {

    const self = this;

    $(window).on('resize', _.debounce(function () {

      const oldSize = self.settings.currentSize;
      const newSize = self._getCurrentState();

      if (oldSize !== newSize) {
        self._updateCurrentState();
        $(document).trigger('changed.bw.mediaquery', [newSize, oldSize]);
      }

    }, 100));


  },

  /**
   * Compares body width and returns the associated breakpoint name
   *
   * @returns {string}
   * @private
   */
  _getCurrentState: function () {

    const currentWidth = $('body').width();

    for (let i = 1; i < this.settings.breakpointWidths.length; i++) {

      if (currentWidth < this.settings.breakpointWidths[i]) {
        return this.settings.breakpoints[i - 1];
      }
    }

    // use last breakpoint
    return this.settings.breakpoints[this.settings.breakpointWidths.length - 1];

  },

  /**
   * Check body with and update the currentSize
   * @private
   */
  _updateCurrentState: function () {

    this.settings.currentSize = this._getCurrentState();

  },

  /**
   * Checks if active breakpoint is at least given breakpoint name
   *
   * @param size
   * @returns {boolean}
   */
  atLeast: function (size) {

    if (this.settings.breakpoints.indexOf(size) === -1) {
      console.error('Breakpoint name not found in settings');
      return false;
    }

    const atLeastBreakpointIndex = this.settings.breakpoints.indexOf(size);
    const currentBreakpointIndex = this.settings.breakpoints.indexOf(this.settings.currentSize);

    return atLeastBreakpointIndex <= currentBreakpointIndex;
  },

  /**
   * Checks if given breakpoint name is currently active
   *
   * @param size
   * @returns {boolean}
   */
  is: function (size) {

    if (this.settings.breakpoints.indexOf(size) === -1) {
      console.error('Breakpoint name not found in settings');
      return false;
    }

    return size === this.settings.currentSize;
  },

  /**
   * Returns the currentSize
   */
  current: function() {
    return this.settings.currentSize;
  },

  /**
   * Default Foundation 6.5.7 breakpoints
   */
  _defaults: {

    breakpoints: ['small', 'medium', 'large', 'xlarge', 'xxlarge'],
    breakpointWidths: [0, 640, 1024, 1200, 1440],
    currentSize: ''

  },


};

$.MediaQuery = new MediaQuery();
