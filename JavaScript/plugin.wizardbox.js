
function Wizardbox(options) {

    // extend the default options
    this.settings = $.extend({}, this._defaults, options);

    this.init();

    return this;
}

Wizardbox.prototype = {

    _defaults: {
        markup: '<div class="bw-wizardbox">' +
            '    <div class="bw-wizardbox__shadow"></div>' +
            '    <div class="bw-wizardbox__wrap">' +
            '        <div class="bw-wizardbox__content">' +
            '        <div class="bw-wizardbox__navigator"><div class="none"></div></div>' +
            '           <div class="bw-wizardbox__container"></div>' +
            '           <div class="bw-wizardbox__loader">' +
            '               <svg width="38" height="38" xmlns="http://www.w3.org/2000/svg" stroke="#fff"><g transform="translate(1 1)" stroke-width="2" fill="none" fill-rule="evenodd"><circle stroke-opacity=".5" cx="18" cy="18" r="18"/><path d="M36 18c0-9.94-8.06-18-18-18"><animateTransform attributeName="transform" type="rotate" from="0 18 18" to="360 18 18" dur="1s" repeatCount="indefinite"/></path></g></svg>' +
            '           </div>' +
            '               <a data-close class="bw-wizardbox__close-link" href="#">' +
            '                <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg">\n' +
            '                <path fill="#7C7C7C"\n' +
            '                      d="M18.416 0L10 8.416 1.584 0 0 1.584 8.416 10 0 18.416 1.584 20 10 11.584 18.416 20 20 18.416 11.584 10 20 1.584z"\n' +
            '                      fill-rule="evenodd"/>\n' +
            '            </svg></a>' +
            '        </div>' +
            '    </div>' +
            '</div>',
        steps: [],
        isCloseable: true,
        showNavigator: true,
        size: 'small',
        dynamicHeight: true,
        fadeIn: true,
        icons: {
            check: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#fff" d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z"/></svg>'
        }
    },

    /**
     * Called on initialization
     */
    init: function () {

        $(document).trigger('bw.wizardbox.beforeInit');
        this._insertMarkup();
        $(document).trigger('bw.wizardbox.beforeCacheDom');
        this._cacheDom();
        $(document).trigger('bw.wizardbox.beforeSetup');
        this._setup();
        $(document).trigger('bw.wizardbox.beforeBindEvent');
        this._bindEvents();
        $(document).trigger('bw.wizardbox.afterInit');

    },

    /**
     * Drop box markup to the DOM on initialization
     * @private
     */
    _insertMarkup: function () {
        // insert basic markup to DOM
        this.$body = $('body');
        this.$body.append(this.settings.markup);
        this.$lightbox = $('.bw-wizardbox');
        this.$container = $('.bw-wizardbox__container', this.$lightbox);
        this.$navigator = $('.bw-wizardbox__navigator', this.$lightbox);

        // insert every step to DOM
        for (let i = 0; i < this.settings.steps.length; i++) {
            this._insertStep(this.settings.steps[i]);
        }

        // insert navigator
        this._refreshNavigator();

        // adjust container width to amount of steps
        this._configureWidth();

        this.$lightbox.addClass('bw-wizardbox--' + this.settings.size);

    },

    /**
     * Add a new Slide to the Wizard and trigger recalculations
     * @param name
     * @param html
     * @param title
     */
    addStep: function (step, position = -1) {

        position = position === -1 ? this.settings.steps.length : position;

        this.settings.steps.splice(position, 0, step);

        this._insertStep(step);
        this._refreshNavigator();
        this._configureWidth();
        this._cacheDom();
        this._bindEvents();

    },

    /**
     * Insert the DOM from all settings.steps to the lightbox markup
     * @param step
     * @private
     */
    _insertStep: function (step) {

        // insert step content
        const stepMarkup = $('<div class="bw-wizardbox__step bw-wizardbox__step--' + step.id + '" data-step="' + step.id + '" data-step-size="' + (step.size ? step.size : this.settings.size) + '"></div>').append(step.content);
        stepMarkup.appendTo(this.$container);

        // insert navigation item
        // filter steps for steps that actually have a title and get the index from that new list
        const offset = this.settings.steps.filter(function (s) {
            return !!s.title;
        }).indexOf(step);

        if (step.title) {
            const navItemContent = step.icon ? this.settings.icons[step.icon] : offset + 1;
            const navItem = $('<div class="bullet">').attr('data-step', step.id)
                .append($('<span class="number">').html(navItemContent)).addClass(step.icon ? 'icon' : '')
                .append($('<span class="check">').html(this.settings.icons.check))
                .append($('<span class="text">').html(step.title));

            if (offset === 0) {
                $('div.none:first-child', this.$navigator).after($('<span class="line">')).after(navItem);
            } else {
                $('div.bullet', this.$navigator).eq(offset - 1).after(navItem).after($('<span class="line">'));
            }
        }

    },

    _refreshNavigator: function () {

        const bulletCount = $('.bullet', this.$navigator).length;

        $('.bullet', this.$navigator).each(function (i, e) {
            // refresh numbers (if no icon)
            if (!$('.number', e).hasClass('icon')) {
                $('.number', e).html(i + 1);
            }
            // hide last line
            $(e).next().removeClass('none').addClass('line');
            if ((i + 1) === bulletCount) {
                $(e).next().addClass('none').removeClass('line');
            }
        });

    },

    /**
     * Set the slider width (steps * 100%)
     * @private
     */
    _configureWidth: function () {
        const stepKeys = Object.keys(this.settings.steps);
        this.$container
            .css('grid-template-columns', 'repeat(' + stepKeys.length + ', 1fr)')
            .css('width', stepKeys.length * 100 + '%');
    },

    /**
     * Set all interactive elements that have been insert
     * @private
     */
    _cacheDom: function () {

        this.$closeLinks = $('[data-close]', this.$lightbox);
        this.$stepLinks = $('[data-to-step]', this.$lightbox);
        this.$bullets = $('.bullet', this.$lightbox);
        this.$steps = $('.bw-wizardbox__step', this.$lightbox);

    },

    /**
     * Configure stuff before event binding
     * @private
     */
    _setup: function () {

        if (this.settings.isCloseable) {
            this.makeClosable();
        }

        if (this.settings.showNavigator) {
            this.showNavigator();
        }

        if (this.settings.dynamicHeight) {
            this.$lightbox.addClass('bw-wizardbox--dynamicHeight');
        }

        if (this.settings.fadeIn) {
            this.$lightbox.addClass('bw-wizardbox--fadeIn');
        }

    },

    /**
     * Bind click events
     * @private
     */
    _bindEvents: function () {
        this.$closeLinks.off('click').on('click', this._onCloseLinkClick.bind(this));
        this.$stepLinks.off('click').on('click', this._onStepLinkClick.bind(this));
    },

    /**
     * Set the active step (optional) and open the box
     * @param stepName
     */
    open: function (stepName = false) {

        if (stepName || 0 === stepName) {
            this.toStep(stepName.toString(), false);
        }

        this.$body.addClass('open-wizardbox');
    },

    /**
     * Event when user clicks anywhere in the document
     * @param e
     * @private
     */
    _onDocumentClick: function (e) {
        const $target = $(e.target);
        if (!$target.closest('.bw-wizardbox__content').length) {
            this.close();
        }
    },

    /**
     * Close the box and trigger event
     */
    close: function () {
        this.$body.removeClass('open-wizardbox');
        this._navigationReset();

        $(document).trigger('bw.wizardbox.closed');
    },

    /**
     * A close link has been clicked
     * @param e
     * @private
     */
    _onCloseLinkClick: function (e) {
        e.preventDefault();
        this.close();
    },

    /**
     * Checks for the escape key
     * @param e
     * @private
     */
    _onDocumentKeyUp: function (e) {
        if (e.key === "Escape") {
            this.close();
        }
    },

    /**
     * Switch the active step of the wizard
     * With or without animation
     * Trigger event @TODO
     * @param stepId
     * @param animate
     * @param markSuccess
     */
    toStep: function (stepId, animate = true, markSuccess = true) {

        // trigger event
        $(document).trigger('bw.wizardbox.beforeChangeStep', stepId, animate, markSuccess);

        const self = this;
        let animationTime = 0;
        const $step = this.$steps.filter('[data-step="' + stepId + '"]');

        // check if step does actually exists
        if (!$step) {
            console.error('Step name "' + stepId + '" not found.');
            return;
        }

        self.setWidth($step.attr('data-step-size'));

        if (animate) {
            const animationWidth = -(100 / Object.keys(this.settings.steps).length);
            this.$container.addClass('bw-wizardbox__container--animate');
            animationTime = 1035;

            // 1. bring step slide in position (as second slide)
            self.$steps.removeClass('active');
            $step.css('grid-area', '1 / 2 / 2 / 3').addClass('active');

            // 2. do animation
            setTimeout(function(){
                self.$container.css('transform', 'translate3d(' + animationWidth + '%, 0, 0)');
            },300);
        }

        // 3. reset (step slide is now first slide)
        setTimeout(function () {
            self.$container.removeClass('bw-wizardbox__container--animate');
            self.$container.css('transform', 'translate3d(0, 0, 0)');
            self.$steps.css('grid-area', 'unset');
            self.$steps.filter('[data-step="' + stepId + '"]').css('grid-area', '1 / 1 / 2 / 2').addClass('active');
        }, animationTime);

        // 4. set title
        this.setNavigationStep(stepId, markSuccess);

        // trigger event
        $(document).trigger('bw.wizardbox.afterChangeStep', stepId, animate, markSuccess);
    },

    /**
     *
     * @param stepName
     * @param markSuccess If true, the currently active point gets highlighted
     */
    setNavigationStep(stepName, markSuccess = true) {

        // stop margin active point success
        if (markSuccess) {
            this.$bullets.filter('.active').addClass('success');
        }

        // remove all active, set only current step active
        this.$bullets.removeClass('active');
        this.$bullets.filter('[data-step="' + stepName + '"]').removeClass('success').addClass('active');

        // reset success lines
        this.$lightbox.find('.line.success').removeClass('success');
        this.$lightbox.find('.bullet.success + .line + .bullet.success').prev().addClass('success');
    },

    /**
     * Remove all active or success items
     * @private
     */
    _navigationReset: function () {
        this.$bullets.removeClass('active').removeClass('success');
        this.$lightbox.find('.line.success').removeClass('success');
    },

    /**
     * Click event for links having [data-to-step]
     * @param e
     * @private
     */
    _onStepLinkClick: function (e) {
        e.preventDefault();
        const link = $(e.currentTarget);
        this.toStep(link.attr('data-to-step'), !(link.attr('data-animate') === "false"), !(link.attr('data-mark-success') === "false"));
    },

    /**
     * Enable Lightbox to be closed via Button and keyboard
     */
    makeClosable: function () {
        // display close button
        this.$lightbox.addClass('bw-wizardbox--closable');

        // bind events
        $(document).on('click', '.bw-wizardbox', this._onDocumentClick.bind(this));
        $(document).on('keyup', this._onDocumentKeyUp.bind(this));
    },

    /**
     * Adjust the width of the Lightbox
     * @param size
     */
    setWidth: function (size) {
        this.$lightbox
            .removeClass('bw-wizardbox--small')
            .removeClass('bw-wizardbox--medium')
            .removeClass('bw-wizardbox--large')
            .addClass('bw-wizardbox--' + size);
    },

    /**
     * Remove closing ability of lightbox
     */
    stopClosable: function () {
        // hide close button
        this.$lightbox.removeClass('bw-wizardbox--closable');

        // unbind events
        $(document).off('click');
        $(document).off('keyup');
    },

    /**
     * Display loader and make box not closeable
     */
    startLoader: function (scrollToTop = true) {
        this.$lightbox.addClass('bw-wizardbox--loading');
        if (scrollToTop) {
            this.$lightbox.find('.bw-wizardbox__content')[0].scrollTop = 0;
        }
        this.stopClosable();
    },

    /**
     * Remove loading animation with delay
     * @param animate
     */
    stopLoader: function (animate = true) {
        const self = this;
        let animationTime = 0;

        // restart closing button
        if (this.settings.isCloseable) {
            this.makeClosable();
        }

        // set waiting
        if (animate) {
            animationTime = 1035;
        }

        // remove css class
        setTimeout(function () {
            self.$lightbox.removeClass('bw-wizardbox--loading');
        }, animationTime);
    },

    /**
     * Enable the Navigator
     */
    showNavigator: function () {
        this.$lightbox.addClass('bw-wizardbox--navigator');
    },

    /**
     * Remove the Navigator
     */
    hideNavigator: function () {
        this.$lightbox.removeClass('bw-wizardbox--navigator');
    }
};

$.Wizardbox = function (options) {
    return new Wizardbox(options);
};
