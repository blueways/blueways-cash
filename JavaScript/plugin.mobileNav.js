function MobileNav(options) {

  // extend default options
  this.settings = $.extend({}, this._defaults, options);

  this.init();

  return this;
}


MobileNav.prototype = {

  _defaults: {
    showFor: ['small'],
    selectorHeader: 'header.topbar2',
    selectorMenuButton: '.menu-button',
    selectorMenuLinks: '.menu.level1 li a',
    isOpenClass: 'is-open'
  },

  init: function () {

    this.cacheDom();

    if (this._isShownFor($.MediaQuery.current())) {
      this.bindEvents();
    }

    this.bindListener();
  },

  cacheDom: function () {
    this.$header = $(this.settings.selectorHeader);
    this.$menuButton = $(this.settings.selectorMenuButton);
    this.$menuLinks = $(this.settings.selectorMenuLinks);
  },

  destroy: function () {
    // remove events
    this.$menuButton.off('click');
    this.$menuLinks.off('click');
    $(document).off('changed.bw.mediaquery', this.reInit);
  },

  bindEvents: function () {

    // Mobile navigation: menu button
    this.$menuButton.on('click', this.onMenuButtonClick.bind(this));

    // Link click in menu
    this.$menuLinks.on('click', this.onMenuLinkClick.bind(this));
  },

  bindListener: function () {

    $(document).on('changed.bw.mediaquery', this.reInit.bind(this));
  },

  reInit: function () {
    this.destroy();
    this.init();
  },

  _isShownFor: function(size) {
    return this.settings.showFor.indexOf(size) !== -1;
  },

  onMenuButtonClick: function (e) {
    e.preventDefault();

    this.$header.toggleClass(this.settings.isOpenClass);

    // reset navigation state
    if (!this.$header.hasClass(this.settings.isOpenClass)) {
      $('.menu-show', this.$header).removeClass('menu-show');
    }

  },

  onMenuLinkClick: function (e) {

    const $link = $(e.currentTarget);

    // navigate to next level
    if ($link.parent().hasClass('is-dropdown-submenu-parent')) {
      e.preventDefault();
      $link.siblings('ul').addClass('menu-show');
    }

    // navigate back
    if ($link.parent().hasClass('back-button')) {
      e.preventDefault();
      $link.parent().parent().removeClass('menu-show');
    }

  }
};

$.MobileNav = function (options) {
  return new MobileNav(options);
};
