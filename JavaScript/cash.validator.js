/**
 * Start the Validator plugin
 *
 * Version 0.1
 *
 * @param options
 * @returns {Validator}
 * @constructor
 */
function Validator(options) {

  // extend default options
  this.settings = $.extend({}, this._defaults, options);

  this.init();

  return this;
}

Validator.prototype = {

  currentForm: null,

  init: function () {

    this._cacheDom();
    this._bindEvents();

  },

  _cacheDom: function () {

    this.submitButton = $(this.settings.selector.submitButton);

  },

  _bindEvents: function () {
    this.submitButton.on('click', this._onFormSubmitClick.bind(this));
  },

  _markInput(input, valid) {
    const $label = $('label[for="' + $(input).attr('id') + '"]');
    if (valid) {
      $label.removeClass('is-invalid-label');
      $(input).removeClass('is-invalid-input');
      $(input).off('change');
    } else {
      $label.addClass('is-invalid-label');
      $(input).addClass('is-invalid-input');
      $(input).off('change').on('change', this._validateInput.bind(this, input));
    }
  },

  _validateInput: function (input) {
    const self = this;
    let isValid = true;

    if ($(input).is('[type="text"][required]')) {
      isValid = $(input).val().length > 2;
      self._markInput(input, isValid);
    }

    if($(input).is('[type="email"][required]')) {
      const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      isValid = re.test(String($(input).val()).toLowerCase());
      self._markInput(input, isValid);
    }

    if ($(input).is('[data-validator="option_selected"]')) {
      isValid = $(input).val() > 0;
      self._markInput(input, isValid);
    }

    if ($(input).is('[data-validator="is_checked"]')) {
      isValid = $(input).is(':checked');
      self._markInput(input, isValid);
    }

    return isValid;
  },

  _checkElements: function (inputs) {
    const self = this;
    let errorCount = 0;

    $.each(inputs, function (i, input) {

      const isValid = self._validateInput(input);
      if (!isValid) {
        errorCount++;
      }

    });

    return errorCount;
  },

  _validateForm: function (form) {

    const inputs = $('input, select', form);
    const errorCount = this._checkElements(inputs);

    return errorCount === 0;
  },

  isValidForm: function () {
    return this._validateForm(this.form);
  },

  _onFormSubmitClick: function (e) {
    this.form = $(e.currentTarget).closest('form');
    const isValid = this._validateForm(this.form);

    if (!isValid) {
      e.preventDefault();
    }
  },

  /**
   * Default Foundation 6.5.7 breakpoints
   */
  _defaults: {
    selector: {
      submitButton: 'form[data-validate="true"] button[type="submit"]',
      closestInputLabelElement: '.grid-x'
    }
  },

};

$.Validator = function (options) {
  return new Validator(options);
};
