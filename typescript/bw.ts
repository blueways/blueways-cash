import $ from 'cash-dom';
import {isEmpty} from "./utils/UnderscoreUtil";
import {LightboxPlugin, LightboxSettings} from "./plugins/LightboxPlugin";
import {WizardBoxPlugin, WizardBoxSettings} from "./plugins/WizardboxPlugin";
import {MediaQueryUtil, MediaQuerySettings} from "./utils/MediaQueryUtil";
import {InterchangeUtil, InterchangeSettings} from "./utils/InterchangeUtil";
import {ValidatorPlugin, ValidatorSettings} from "./plugins/ValidatorPlugin";
import {CalendarPlugin, CalendarSettings} from "./plugins/CalendarPlugin";

export interface BwSettings {
    MediaQuery?: MediaQuerySettings,
    Interchange?: InterchangeSettings,
    WizardBox?: WizardBoxSettings,
    Lightbox?: LightboxSettings,
    Validator?: ValidatorSettings
}

export let MediaQuery = new MediaQueryUtil();
export let Interchange = new InterchangeUtil();
export const Lightbox = LightboxPlugin;
export const WizardBox = WizardBoxPlugin;
export const Validator = ValidatorPlugin;
export const Calendar = CalendarPlugin;

export default class Bw {
    public Lightbox: LightboxPlugin;
    public WizardBox: WizardBoxPlugin;
    public Validator: ValidatorPlugin;

    // fix to access from other files
    static MediaQuery: MediaQueryUtil;

    constructor(settings?: BwSettings) {
        settings = $.extend({}, settings);

        this.updateUtils(settings);
        this.initPlugins(settings);

        return this;
    }

    private updateUtils(settings: BwSettings) {
        MediaQuery.update(settings.MediaQuery);
        Interchange.update(settings.Interchange);
    }

    private initPlugins(settings: BwSettings) {
        if (!isEmpty(settings.Lightbox)) {
            this.Lightbox = new LightboxPlugin(settings.Lightbox);
        }

        if (!isEmpty(settings.WizardBox)) {
            this.WizardBox = new WizardBoxPlugin(settings.WizardBox);
        }

        if (!isEmpty(settings.Validator)) {
            this.Validator = new ValidatorPlugin(settings.Validator);
        }

    }
}

$.fn.extend({
    bw: (settings?: BwSettings) => {
        settings = $.extend({}, settings);
        return new Bw(settings);
    },
    calendar: function (settings?: CalendarSettings) {
        return new CalendarPlugin(this, settings);
    }
});
