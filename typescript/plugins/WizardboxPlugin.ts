import $, {Cash} from 'cash-dom'
import '../../Scss/plugins/bw.wizardBox.scss'

enum WizardBoxSize {
    Small = 'small',
    Medium = 'medium',
    Large = 'large',
    Full = 'full'
}

export interface WizardBoxIcon {
    identifier: string,
    markup: string
}

export interface WizardBoxStep {
    id: string | number,
    content: string,
    size?: WizardBoxSize,
    icon?: string,
    title?: string
}

export interface WizardBoxSettings {
    markup?: string,
    steps?: Array<WizardBoxStep>,
    isClosable?: boolean,
    showNavigator?: boolean,
    size?: WizardBoxSize.Small,
    dynamicHeight?: boolean,
    fadeIn?: true,
    icons?: Array<WizardBoxIcon>
}

const defaults: WizardBoxSettings = {
    markup: '<div class="bw-wizardBox">' +
        '    <div class="bw-wizardBox__shadow"></div>' +
        '    <div class="bw-wizardBox__wrap">' +
        '        <div class="bw-wizardBox__content">' +
        '        <div class="bw-wizardBox__navigator"><div class="none"></div></div>' +
        '           <div class="bw-wizardBox__container"></div>' +
        '           <div class="bw-wizardBox__loader">' +
        '               <svg width="38" height="38" xmlns="http://www.w3.org/2000/svg" stroke="#fff"><g transform="translate(1 1)" stroke-width="2" fill="none" fill-rule="evenodd"><circle stroke-opacity=".5" cx="18" cy="18" r="18"/><path d="M36 18c0-9.94-8.06-18-18-18"><animateTransform attributeName="transform" type="rotate" from="0 18 18" to="360 18 18" dur="1s" repeatCount="indefinite"/></path></g></svg>' +
        '           </div>' +
        '               <a data-close class="bw-wizardBox__close-link" href="#">' +
        '                <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg">\n' +
        '                <path fill="#7C7C7C"\n' +
        '                      d="M18.416 0L10 8.416 1.584 0 0 1.584 8.416 10 0 18.416 1.584 20 10 11.584 18.416 20 20 18.416 11.584 10 20 1.584z"\n' +
        '                      fill-rule="evenodd"/>\n' +
        '            </svg></a>' +
        '        </div>' +
        '    </div>' +
        '</div>',
    steps: [],
    isClosable: true,
    showNavigator: false,
    size: WizardBoxSize.Small,
    dynamicHeight: false,
    fadeIn: true,
    icons: [
        {
            identifier: 'check',
            markup: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#fff" d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z"/></svg>'
        }
    ]
};

export class WizardBoxPlugin {

    private readonly settings: WizardBoxSettings;
    private $body: Cash;
    private $lightbox: Cash;
    private $container: Cash;
    private $navigator: Cash;
    private $closeLinks: Cash;
    private $stepLinks: Cash;
    private $bullets: Cash;
    private $steps: Cash;

    constructor(options?: WizardBoxSettings) {
        this.settings = $.extend(defaults, this.settings, options);
        this.init();
    }

    private init() {
        $(document).trigger('bw.wizardBox.beforeInit');
        this.insertMarkup();
        $(document).trigger('bw.wizardBox.beforeCacheDom');
        this.cacheDom();
        $(document).trigger('bw.wizardBox.beforeSetup');
        this.setup();
        $(document).trigger('bw.wizardBox.beforeBindEvent');
        this.bindEvents();
        $(document).trigger('bw.wizardBox.afterInit');
    }

    private insertMarkup() {
        // insert basic markup to DOM
        this.$body = $('body');
        this.$body.append(this.settings.markup);
        this.$lightbox = $('.bw-wizardBox');
        this.$container = $('.bw-wizardBox__container', this.$lightbox);
        this.$navigator = $('.bw-wizardBox__navigator', this.$lightbox);

        // insert every step to DOM
        for (let i = 0; i < this.settings.steps.length; i++) {
            this.insertStep(this.settings.steps[i]);
        }

        // insert navigator
        this.refreshNavigator();

        // adjust container width to amount of steps
        this.configureWidth();

        this.$lightbox.addClass('bw-wizardBox--' + this.settings.size);
    }

    private insertStep(step: WizardBoxStep) {

        // insert step content
        const stepMarkup = $('<div class="bw-wizardBox__step bw-wizardBox__step--' + step.id + '" data-step="' + step.id + '" data-step-size="' + (step.size ? step.size : this.settings.size) + '"></div>').append(step.content);
        stepMarkup.appendTo(this.$container);

        // insert navigation item
        // filter steps for steps that actually have a title and get the index from that new list
        const offset = this.settings.steps.filter(function (s) {
            return !!s.title;
        }).indexOf(step);

        if (step.title) {
            const navItemContent = step.icon ? this.getIcon(step.icon).markup : (offset + 1).toString();
            const navItem = $('<div class="bullet">').attr('data-step', step.id.toString())
                .append($('<span class="number">').html(navItemContent)).addClass(step.icon ? 'icon' : '')
                .append($('<span class="check">').html(this.getIcon('check').markup))
                .append($('<span class="text">').html(step.title));

            if (offset === 0) {
                $('div.none:first-child', this.$navigator).after($('<span class="line">')).after(navItem);
            } else {
                $('div.bullet', this.$navigator).eq(offset - 1).after(navItem).after($('<span class="line">'));
            }
        }

    }

    private getIcon(identifier: string): WizardBoxIcon {
        return this.settings.icons.find((icon) => {
            return icon.identifier === identifier;
        });
    }

    private refreshNavigator() {
        const bulletCount = $('.bullet', this.$navigator).length;

        $('.bullet', this.$navigator).each(function (i, e) {
            // refresh numbers (if no icon)
            if (!$('.number', e).hasClass('icon')) {
                $('.number', e).html((i + 1).toString());
            }
            // hide last line
            $(e).next().removeClass('none').addClass('line');
            if ((i + 1) === bulletCount) {
                $(e).next().addClass('none').removeClass('line');
            }
        });
    }

    private configureWidth() {
        const stepKeys = Object.keys(this.settings.steps);
        this.$container
            .css('grid-template-columns', 'repeat(' + stepKeys.length + ', 1fr)')
            .css('width', stepKeys.length * 100 + '%');
    }

    private cacheDom() {
        this.$closeLinks = $('[data-close]', this.$lightbox);
        this.$stepLinks = $('[data-to-step]', this.$lightbox);
        this.$bullets = $('.bullet', this.$lightbox);
        this.$steps = $('.bw-wizardBox__step', this.$lightbox);
    }

    private setup() {
        if (this.settings.isClosable) {
            this.makeClosable();
        }

        if (this.settings.showNavigator) {
            this.showNavigator();
        }

        if (this.settings.dynamicHeight) {
            this.$lightbox.addClass('bw-wizardBox--dynamicHeight');
        }

        if (this.settings.fadeIn) {
            this.$lightbox.addClass('bw-wizardBox--fadeIn');
        }
    }

    private makeClosable() {
        // display close button
        this.$lightbox.addClass('bw-wizardBox--closable');

        // bind events
        $(document).on('click', '.bw-wizardBox', (e) => this.onDocumentClick(e));
        $(document).on('keyup', (e) => this.onDocumentKeyUp(e));
    }

    close() {
        this.$body.removeClass('open-wizardBox');
        this.navigationReset();

        $(document).trigger('bw.wizardBox.closed');
    }

    open(stepName?: string) {
        if (stepName) {
            this.toStep(stepName.toString(), false);
        }

        this.$body.addClass('open-wizardBox');
    }

    private onDocumentClick(e: any) {
        const $target = $(e.target);
        if (!$target.closest('.bw-wizardBox__content').length) {
            this.close();
        }
    }

    private onDocumentKeyUp(e: any) {
        if (e.key === "Escape") {
            this.close();
        }
    }

    private navigationReset() {
        this.$bullets.removeClass('active').removeClass('success');
        this.$lightbox.find('.line.success').removeClass('success');
    }

    showNavigator() {
        this.$lightbox.addClass('bw-wizardBox--navigator');
    }

    hideNavigator() {
        this.$lightbox.removeClass('bw-wizardBox--navigator');
    }

    private bindEvents() {
        this.$closeLinks.off('click').on('click', (e) => this.onCloseLinkClick(e));
        this.$stepLinks.off('click').on('click', (e) => this.onStepLinkClick(e));
    }

    private onCloseLinkClick(e: any) {
        e.preventDefault();
        this.close();
    }

    private onStepLinkClick(e: any) {
        e.preventDefault();
        const link = $(e.currentTarget);
        this.toStep(link.attr('data-to-step'), !(link.attr('data-animate') === "false"), !(link.attr('data-mark-success') === "false"));
    }

    private toStep(stepId: string, animate: boolean, markSuccess: boolean = true) {
        // trigger event
        $(document).trigger('bw.wizardBox.beforeChangeStep', [stepId, animate, markSuccess]);

        let animationTime = 0;
        const $step = this.$steps.filter('[data-step="' + stepId + '"]');

        // check if step does actually exists
        if (!$step) {
            console.error('Step name "' + stepId + '" not found.');
            return;
        }

        this.setWidth($step.attr('data-step-size'));

        if (animate) {
            const animationWidth = -(100 / Object.keys(this.settings.steps).length);
            this.$container.addClass('bw-wizardBox__container--animate');
            animationTime = 1035;

            // 1. bring step slide in position (as second slide)
            this.$steps.removeClass('active');
            $step.css('grid-area', '1 / 2 / 2 / 3').addClass('active');

            // 2. do animation
            setTimeout(() => {
                this.$container.css('transform', 'translate3d(' + animationWidth + '%, 0, 0)');
            }, 300);
        }

        // 3. reset (step slide is now first slide)
        setTimeout(() => {
            this.$container.removeClass('bw-wizardBox__container--animate');
            this.$container.css('transform', 'translate3d(0, 0, 0)');
            this.$steps.css('grid-area', 'unset');
            this.$steps.filter('[data-step="' + stepId + '"]').css('grid-area', '1 / 1 / 2 / 2').addClass('active');
        }, animationTime);

        // 4. set title
        this.setNavigationStep(stepId, markSuccess);

        // trigger event
        $(document).trigger('bw.wizardBox.afterChangeStep', [stepId, animate, markSuccess]);
    }

    private setWidth(size: string) {
        this.$lightbox
            .removeClass('bw-wizardBox--small')
            .removeClass('bw-wizardBox--medium')
            .removeClass('bw-wizardBox--large')
            .addClass('bw-wizardBox--' + size);
    }

    private setNavigationStep(stepId: string, markSuccess: boolean) {
        // stop margin active point success
        if (markSuccess) {
            this.$bullets.filter('.active').addClass('success');
        }

        // remove all active, set only current step active
        this.$bullets.removeClass('active');
        this.$bullets.filter('[data-step="' + stepId + '"]').removeClass('success').addClass('active');

        // reset success lines
        this.$lightbox.find('.line.success').removeClass('success');
        this.$lightbox.find('.bullet.success + .line + .bullet.success').prev().addClass('success');
    }

    addStep(step: WizardBoxStep, position = -1) {
        position = position === -1 ? this.settings.steps.length : position;

        this.settings.steps.splice(position, 0, step);

        this.insertStep(step);
        this.refreshNavigator();
        this.configureWidth();
        this.cacheDom();
        this.bindEvents();
    }
}
