import $, {Cash} from 'cash-dom';
import '../../Scss/plugins/bw.lightbox.scss'

const defaults: LightboxSettings = {
    id: 'default',
    markup: '<div class="bw-lightbox">' +
        '    <div class="bw-lightbox__shadow"></div>' +
        '    <div class="bw-lightbox__wrap">' +
        '        <div class="bw-lightbox__content">' +
        '            <a class="bw-lightbox__nav-link bw-lightbox__nav-link--prev" href="#"><</a>' +
        '            <div class="bw-lightbox__content-drop">' +
        '            </div>' +
        '            <a class="bw-lightbox__nav-link bw-lightbox__nav-link--next" href="#">></a>' +
        '            <a class="bw-lightbox__close-link" href="#">' +
        '                <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg">\n' +
        '                <path fill="#7C7C7C"\n' +
        '                      d="M18.416 0L10 8.416 1.584 0 0 1.584 8.416 10 0 18.416 1.584 20 10 11.584 18.416 20 20 18.416 11.584 10 20 1.584z"\n' +
        '                      fill-rule="evenodd"/>\n' +
        '            </svg></a>' +
        '        </div>' +
        '    </div>' +
        '</div>',
    selector: 'a.lightbox',
};


export interface LightboxSettings {
    id?: string,
    markup?: string,
    selector?: string
}

export class LightboxPlugin {

    private readonly settings: LightboxSettings;
    private $body: Cash;
    private $elements: Cash;
    private $lightbox: Cash;
    private $navLinks: Cash;
    private $closeLink: Cash;
    private $wrap: Cash;
    private $content: Cash;
    private $currentImages: Cash;
    private currentImageOffset: number;


    constructor(options?: LightboxSettings) {
        this.settings = $.extend(defaults, this.settings, options);
        this.init();
    }

    private init() {
        this.cacheDom();
        this.bindEvents();
    }

    private cacheDom() {
        this.$body = $('body');

        if (!this.settings.id) {
            this.settings.id = '_' + Math.random().toString(36).substr(2, 9);
        }

        $('.bw-lightbox').attr('id', this.settings.id);
        this.$body.append(this.settings.markup);
        this.$elements = $(this.settings.selector);
        this.$lightbox = $('#' + this.settings.id);
        this.$navLinks = $('a.bw-lightbox__nav-link', this.$lightbox);
        this.$closeLink = $('a.bw-lightbox__close-link', this.$lightbox);
        this.$wrap = $('.bw-lightbox__wrap', this.$lightbox);
        this.$content = $('.bw-lightbox__content-drop', this.$lightbox);
    }

    private bindEvents() {
        this.$elements.on('click', (e) => this.onElementClick(e));
        this.$navLinks.on('click', (e) => this.onNavLinkClick(e));
        this.$closeLink.on('click', (e) => this.onCloseLinkClick(e));
    }

    private onElementClick(e: any) {
        e.preventDefault();
        const $link = $(e.currentTarget);
        const imageUri = $link.attr('href');

        this.$currentImages = this.$elements.filter('[rel="' + $link.attr('rel') + '"]');
        this.currentImageOffset = this.$currentImages.index($link);

        this.showImage(imageUri);
        this.openLightbox(true);
    }

    private showImage(imageUri: string) {
        const image = '<img src="' + imageUri + '" />';
        this.$content.html(image);
    }

    private openLightbox(isImage: boolean) {
        this.$body.addClass('open-lightbox');
        if (isImage) {
            this.$lightbox.addClass('is-image-lightbox')
        } else {
            this.$lightbox.addClass('is-content-lightbox')
        }
        this.bindClosingAbility();
    }

    private bindClosingAbility() {
        $(document).on('click', '.bw-lightbox', (e) => this.onDocumentClick(e));
        $(document).on('keyup', (e) => this.onDocumentKeyUp(e));
    }

    private onDocumentClick(e: any) {
        const $target = $(e.target);
        if (!$target.closest('.bw-lightbox__content').length) {
            this.closeLightbox();
        }
    }

    private onDocumentKeyUp(e: any) {
        if (e.key === "Escape") {
            this.closeLightbox();
        }

        if (e.key === "ArrowLeft") {
            const next = (this.currentImageOffset + (this.$currentImages.length - 1)) % this.$currentImages.length;
            this.showNextImage(next);
        }

        if (e.key === "ArrowRight") {
            const next = (this.currentImageOffset + 1) % this.$currentImages.length;
            this.showNextImage(next);
        }
    }

    private closeLightbox() {
        this.$body.removeClass('open-lightbox');
        this.$lightbox.removeClass('is-image-lightbox');
        this.$lightbox.removeClass('is-content-lightbox');
        $(document).off('click');
        $(document).off('keyup');
        $(document).trigger('closed.bw.lightbox');
    }

    private showNextImage(next: number) {

        if (!(this.$currentImages.length <= next)) {
            return;
        }

        const link = this.$currentImages[next];

        this.showImage($(link).attr('href'));

        this.currentImageOffset = next;
    }

    private onNavLinkClick(e: any) {
        e.preventDefault();

        let next = (this.currentImageOffset + 1) % this.$currentImages.length;
        if ($(e.currentTarget).hasClass('bw-lightbox__nav-link--prev')) {
            next = (this.currentImageOffset + (this.$currentImages.length - 1)) % this.$currentImages.length;
        }

        this.showNextImage(next);
    }

    private onCloseLinkClick(e: any) {
        e.preventDefault();
        this.closeLightbox();
    }
}
