import $, {Cash} from 'cash-dom';

const defaults: ValidatorSettings = {
    selector: {
        submitButton: 'form[data-validate="true"] button[type="submit"]',
    }
};

export interface ValidatorSettings {
    selector?: {
        submitButton?: String,
    }
}

export class ValidatorPlugin {

    public $form: Cash;
    public settings: ValidatorSettings;
    public $submitButton: Cash;

    constructor(settings?: ValidatorSettings) {
        this.settings = $.extend({}, defaults, settings);
        this.init();
    }

    public init() {
        this.cacheDom();
        this.bindEvents();
    }

    private cacheDom() {
        // @ts-ignore
        this.$submitButton = $(this.settings.selector.submitButton);
    }

    private bindEvents() {
        this.$submitButton.on('click', this.onFormSubmitClick.bind(this));
    }

    private markInput($input: Cash, valid: boolean) {
        const $label = $('label[for="' + $input.attr('id') + '"]');
        if (valid) {
            $label.removeClass('is-invalid-label');
            $input.removeClass('is-invalid-input');
            $input.off('change');
        } else {
            $label.addClass('is-invalid-label');
            $input.addClass('is-invalid-input');
            $input.off('change').on('change', this.validateInput.bind(this, $input));
        }
    }

    public validateInput($input: Cash): boolean {
        const self = this;
        let isValid = true;

        if ($input.is('[type="text"][required]')) {
            isValid = $input.val().length > 2;
            self.markInput($input, isValid);
        }

        if($input.is('[type="email"][required]')) {
            const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            isValid = re.test(String($input.val()).toLowerCase());
            self.markInput($input, isValid);
        }

        if ($input.is('[data-validator="option_selected"]')) {
            isValid = parseInt(<string>$input.val()) > 0;
            self.markInput($input, isValid);
        }

        if ($input.is('[data-validator="is_checked"]')) {
            isValid = $input.is(':checked');
            self.markInput($input, isValid);
        }

        return isValid;
    }

    private checkElements($inputs: Cash): number {
        const self = this;
        let errorCount = 0;

        $.each($inputs, function (i, input) {

            const isValid = self.validateInput($(input));
            if (!isValid) {
                errorCount++;
            }
        });

        return errorCount;
    }

    public validateForm($form: Cash) {
        const $inputs = $('input, select', $form);
        const errorCount = this.checkElements($inputs);

        return errorCount === 0;
    }

    private onFormSubmitClick(e: any) {
        this.$form = $(e.currentTarget).closest('form');
        const isValid = this.validateForm(this.$form);

        if (!isValid) {
            e.preventDefault();
        }
    }

}
