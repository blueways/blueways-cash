import $, {Cash} from 'cash-dom';
import '../../Scss/plugins/bw.calendar.scss'
import {CalendarResponse, loadJson} from "../utils/BookingManagerUtil";
import {CallbackMapping, ControlElement, ControlElements, ControlSettings, ControlType} from "../utils/FormControlUtil";
import {camelCaseObjectKeys} from "../utils/UnderscoreUtil";
import * as Handlebars from 'handlebars';
import dayjs from 'dayjs';
import HelperDelegate = Handlebars.HelperDelegate;

require('dayjs/locale/de');
dayjs.locale('de');

enum CalendarView {
    list,
    week,
    month,
    day
}

const defaults: CalendarSettings = {
    $context: $(document),
    onloadDate: new Date(),
    baseUri: location.origin,
    calendarUids: [],
    dayCount: 10,
    listViewSize: 10,
    view: CalendarView.list,

    selectors: {
        calendarLinks: 'a[href^="#calendar-"]',
        bookingForm: 'form',
        calendarNameField: 'span[data-calendar-name]',
        inputCalendarUid: 'input[name="tx_bwbookingmanager_api[newEntry][calendar]"]',
        inputStartDate: 'input[name="tx_bwbookingmanager_api[newEntry][startDate]"]',
        inputEndDate: 'input[name="tx_bwbookingmanager_api[newEntry][endDate]"]'
    },
    markup: {
        loadingIcon: '<svg class="bw-calendar__spinner" width="38" height="38" xmlns="http://www.w3.org/2000/svg" stroke="#000"><g transform="translate(1 1)" stroke-width="2" fill="none" fill-rule="evenodd"><circle stroke-opacity=".5" cx="18" cy="18" r="18"/><path d="M36 18c0-9.94-8.06-18-18-18"><animateTransform attributeName="transform" type="rotate" from="0 18 18" to="360 18 18" dur="1s" repeatCount="indefinite"/></path></g></svg>',
        skeleton: '<form class="bw-calendar__controls"></form><div class="bw-calendar__canvas"></div>',
        header: '<div class="bw-calendar__header"></div>',
        body: '<div class="bw-calendar__body"></div>',
        footer: '<div class="bw-calendar__footer"></div>',
    },
    template: {
        list: '<table><thead><tr><th colspan="2">Datum</th><th>Event</th></tr></thead><tbody>' +
            '{{#each days}}' +
            '{{>listDay day=.}}' +
            '{{/each}}</tbody></table>' +
            '<div><a href="#" class="loadMore"><b>Mehr laden</b></a></div>',
        week: '<div class="bw-calendar bw-calendar__week">' +
            '{{#each days}}{{>weekDay day=.}}{{/each}}' +
            '</div>',
        month: '<div class="bw-calendar__month"><strong>Mo</strong><strong>Di</strong><strong>Mi</strong><strong>Do</strong><strong>Fr</strong><strong>Sa</strong><strong>So</strong>' +
            '{{#each days}}<div class="{{dayClassStrings this}}" {{#if @first}}data-start="{{formatDate date "d"}}"{{/if}}>{{formatDate date "D"}}' +
            '{{#each Events}}<a href="#"></a>{{/each}}</div>{{/each}}' +
            '</div>',
        partials: {
            listDay: '<tr class="{{#isCurrentDay}}today{{/isCurrentDay}}"><td><strong>{{formatDate date "dd"}}</strong></td><td>{{formatDate date "DD.MM.YY"}}</td><td>' +
                '{{#each Events}} {{name}} {{/each}}' +
                '{{#if Events}}{{else}}-{{/if}}' +
                '</td></tr>',
            weekDay: '<div>{{formatDate date "dd"}}<br />{{formatDate date "DD.MM."}}</div>',
            monthDay: '<div class="">{{formatDate date "D"}}{{#each Events}}<a href="#">g</a>{{/each}}</div>'
        },
        helper: {
            formatDate: function (dateTime: Date, format: string) {
                return dayjs(dateTime).format(format);
            },
            dayClassStrings: function (day: CalendarDay) {
                let classes = '';
                Object.getOwnPropertyNames(day).forEach((attr) => {
                    // @ts-ignore
                    if (typeof day[attr] === 'boolean' && day[attr]) {
                        classes += attr + ' ';
                    }
                });
                return classes;
            }
        }
    },
    controls: [
        {
            type: ControlType.Radio,
            label: 'Ansicht',
            options: [{
                label: 'Liste',
                value: 'list',
            }, {
                label: 'Woche',
                value: 'week'
            }, {
                label: 'Monat',
                value: 'month'
            }],
            name: 'changeView'
        },
        {
            type: ControlType.Checkbox,
            label: 'Gebuchte Einträge anzeigen',
            name: 'changeBookedView',
            options: [
                {label: 'Ja', value: '1'},
            ]
        }
    ]
};

export interface TemplateHelperFunctions {
    [key: string]: HelperDelegate
}

export interface TemplatePartials {
    [key: string]: string
}

export interface CalendarSettings {
    $context?: Cash
    baseUri: string
    view: CalendarView
    calendarUids: Array<number>,
    onloadDate: Date,
    dayCount?: number,
    listViewSize?: number,
    selectors?: {
        calendarLinks?: string
        bookingForm?: string
        loginForm?: string
        calendarNameField: string
        inputCalendarUid: string
        inputStartDate: string
        inputEndDate: string
    },
    markup?: {
        loadingIcon?: string,
        skeleton?: string,
        header?: string;
        body?: string,
        footer?: string,
    },
    template?: {
        list?: string,
        month?: string,
        week?: string,
        partials?: TemplatePartials,
        helper?: TemplateHelperFunctions
    }
    controls?: Array<ControlSettings>
}

export interface CalendarEvent {
    name: string
    id: string
    startDate: Date
    endDate: Date
    location?: string
    description?: string
    isAllDay?: boolean
    url?: string
    data?: Object
    isAttached?: boolean
}

export class CalendarDay {
    public date: Date;
    public isCurrentDay: boolean;
    public isInPast: boolean;
    public Events: Array<CalendarEvent> = [];
}

interface CalendarYear {
    [months: number]: Array<CalendarDay>
}

interface CalendarYears {
    [year: number]: CalendarYear
}

export class CalendarPlugin {

    private $context: Cash;
    private settings: CalendarSettings;
    private $calendarWrapper: Cash;
    private $controls: Cash;
    private $canvas: Cash;
    private Controls: Array<ControlElement> = [];
    private years: CalendarYears = {};
    private Events: Array<CalendarEvent> = [];

    constructor(context: any, options?: CalendarSettings) {
        this.$context = $(context);
        this.settings = $.extend({}, defaults, this.getSettingsFromContext(), options);
        this.init();
    }

    /**
     * Extract data-Attribute settings in CalendarSettings format
     */
    private getSettingsFromContext() {
        let attributeSettings = this.$context.data();
        attributeSettings = camelCaseObjectKeys(attributeSettings);

        // "1,2,3" => [1, 2, 3]
        if (attributeSettings.hasOwnProperty('calendarUids')) {
            attributeSettings.calendarUids = attributeSettings.calendarUids.toString().split(',').map(function (item: string) {
                return parseInt(item);
            });
        }

        return attributeSettings;
    }

    private init() {

        // register Handlebar helper and partials
        Handlebars.registerHelper(this.settings.template.helper);
        Object.keys(this.settings.template.partials).forEach((name) => {
            Handlebars.registerPartial(name, this.settings.template.partials[name]);
        });

        this.cacheDom();
        this.buildControls();
        this.drawCalendar();

        //this.onLoad();
    }

    private addMonth(year: number, month: number) {

        if (!this.years.hasOwnProperty(year)) {
            this.years[year] = {};
        }

        if (!this.years[year].hasOwnProperty(month)) {
            this.years[year][month] = [];
        }

        let days: Array<CalendarDay> = [];
        let now = new Date();
        now.setHours(0, 0, 0, 0);
        const daysInMonth = new Date(year, month + 1, 0).getDate();

        for (let i = 1; i <= daysInMonth; i++) {
            const date = new Date(year, month, i, 0, 0, 0, 0);
            const isInPast = date < now;
            const isCurrentDay = date.getTime() === now.getTime();

            const day = new CalendarDay();
            day.date = date;
            day.isInPast = isInPast;
            day.isCurrentDay = isCurrentDay;

            this.years[year][month].push(day);
        }

    }

    private getDayRange(startDate: Date, count: number) {
        const year = startDate.getFullYear();
        const month = startDate.getMonth();
        const day = startDate.getDate();

        // add new month if not there
        if (!this.years.hasOwnProperty(year) || !this.years[year].hasOwnProperty(month)) {
            this.addMonth(year, month);
        }

        // get whole month, remove until desired start date
        let days = this.years[year][month].slice(day - 1);

        // remove days if to many
        if (days.length > count) {
            days = days.slice(0, count);
        }

        // call again to add whole new month
        if (days.length < count) {
            const nextDate = month === 12 ? new Date(year + 1, 1, 1) : new Date(year, month + 1, 1);
            const nextCount = count - days.length;
            days = [].concat(days, this.getDayRange(nextDate, nextCount));
        }

        return days;
    }

    private drawCalendar(date?: Date) {

        // clear canvas
        this.$canvas.html('');
        let templateString = '';
        let dayCount = 0;
        date = date ? date : new Date();


        switch (this.settings.view) {
            case CalendarView.week:

                this.settings.onloadDate = dayjs(date).day(1).toDate();
                dayCount = 7;
                templateString = this.settings.template.week;

                break;

            case CalendarView.list:

                dayCount = this.settings.listViewSize;
                templateString = this.settings.template.list;

                break;

            case CalendarView.month:

                date = new Date(date.getFullYear(), date.getMonth(), 1);
                dayCount = dayjs(date).daysInMonth();
                templateString = this.settings.template.month;

                break;
        }

        const template = Handlebars.compile(templateString);
        const days = this.getDayRange(date, dayCount);
        const output = template({days: days, settings: this.settings});
        this.$canvas.html(output);

        this.bindEvents();
    }

    private drawCalendarDays() {

    }

    private showDate() {
    }

    private cacheDom() {
        this.$calendarWrapper = this.$context.addClass('bw-calendar');
        this.$calendarWrapper.html(this.settings.markup.skeleton);
        this.$controls = $('.bw-calendar__controls', this.$calendarWrapper);
        this.$canvas = $('.bw-calendar__canvas', this.$calendarWrapper);
    }

    private bindEvents() {
        const self = this;
        //this.$calendarLinks.on('click', this.onCalendarLinkClick.bind(this));

        this.$canvas.find('.loadMore').on('click', (e) => {
            e.preventDefault();

            this.settings.listViewSize += this.settings.dayCount;
            this.drawCalendar();
        });
    }

    private onLoad() {
        // check for calendar wrapper
        if (!this.$calendarWrapper.length) {
            return;
        }

        // loading animation
        $(this.$calendarWrapper).append(this.settings.markup.loadingIcon).addClass('bw-calendar--loading');

        // start building the calendar
        // @TODO: merge configs and build markup once
        for (let i = 0; i < this.settings.calendarUids.length; i++) {
            const url = '/api/calendar/' + this.settings.calendarUids[i] + '.json';
            //this.loadCalendar(this.settings.calendarUids[i], this.buildCalendarMarkup.bind(this, url));
        }
    }

    private loadCalendar(uid: number, callback?: Function) {
        const calendarUri = this.settings.baseUri + '/api/calendar/' + uid + '.json';
        loadJson(calendarUri, (data: CalendarResponse) => {
            callback(data);
        });
    }

    public onChangeView(event: any) {

        const view: string = $(event.target).val() as string;
        // @ts-ignore
        this.settings.view = CalendarView[view];
        this.drawCalendar();
    }

    public onChangeBookedView(event: any) {
        console.log('change book view', this);
    }

    public matchEvents() {

        const self = this;

        function attachEventToDate(event: CalendarEvent, date: Date) {

            const year = date.getFullYear();
            const month = date.getMonth();
            const day = date.getDate();

            if (!self.years.hasOwnProperty(year) || !self.years[year].hasOwnProperty(month)) {
                self.addMonth(year, month);
            }

            self.years[year][month][day - 1].Events.push(event);
        }

        this.Events.forEach((event) => {
            if (event.isAttached) {
                return;
            }

            // attach to start date
            attachEventToDate(event, event.startDate);

            // check for more days to attach to
            let newStartDate = new Date(event.startDate.getFullYear(), event.startDate.getMonth(), event.startDate.getDate());
            let duration = dayjs(event.endDate).diff(newStartDate, 'day');
            while (duration !== 0) {
                newStartDate = dayjs(newStartDate).add(1, 'day').toDate();
                duration = dayjs(event.endDate).diff(newStartDate, 'day');
                attachEventToDate(event, newStartDate);
            }

            // set as attached
            event.isAttached = true;
        });
    }

    public addEvent(event: CalendarEvent, refresh?: boolean) {

        this.Events.push(event);
        this.matchEvents();

        if (refresh) {
            this.drawCalendar();
        }

    }

    /**
     * Create calendar control elements
     */
    private buildControls() {

        const self = this;

        if (!this.settings.controls.length) {
            return;
        }

        // map the control name to method of this class
        const callBacks: CallbackMapping = {
            changeView: self.onChangeView,
            changeBookedView: self.onChangeBookedView
        };

        this.settings.controls.forEach((setting) => {
            let control: ControlElement = new ControlElements[setting.type](setting);

            // update callback to current instance
            const callback = callBacks[setting.name];
            if (typeof callback == 'function') {
                control.settings.onChange = callback.bind(this);
            }

            // insert element
            control.$element.appendTo(this.$controls);
            this.Controls.push(control);
        });
    }
}
