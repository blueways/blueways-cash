export interface CalendarResponse {
    configuration: {
        timeslots: Array<TimeslotData>
        entries: Array<EntryData>
        days: Array<DayData>
        weeks: Array<Array<number>>
        next: MoreLink
        prev: MoreLink
    }
    calendar: CalendarData,
    user: null|UserData,
    title: String
    message: String
}

export interface CalendarData {
    directBooking: boolean
    name: String
    pid: number
    uid: number
}

export interface UserData {

}

export interface TimeslotData {
    uid: number,
    startDate: number
    displayStartDate: number
    endDate: number,
    displayEndDate: number
    maxWeight: number
    isBookable: boolean
    freeWeight: number
    feUsers: Array<UserData>
    entries: Array<number>
}

export interface EntryData {

}

export interface DayData {
    date: number
    entries: Array<number>
    timeslots: Array<number>
    isCurrentDay: boolean
    isNotInMonth: boolean
    isInPast: boolean
    isSelectedDay: boolean
    bookableTimeslotsStatus: number
    hasBookableTimeslots: boolean
    isDirectBookable: boolean
    isBookable: boolean
}

export interface MoreLink {
    date: string
    day: string
    month: string
    year: string
    link: string
}

export function loadJson(path: string, callback: Function) {
    let httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function () {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
                const data = JSON.parse(httpRequest.responseText);
                if (callback) callback(data);
            }
        }
    };
    httpRequest.open('GET', path);
    httpRequest.send();
}
