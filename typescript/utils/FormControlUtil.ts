import $, {Cash} from 'cash-dom';
import {ID} from "./UnderscoreUtil";

export interface ControlElement {

    defaults: ControlSettings;
    settings: ControlSettings;
    $element: Cash;

    createElement(): void

    bindEvents(): void
}

export enum ControlType {
    Checkbox = 'CheckboxControl',
    Radio = 'RadioControl'
}

export interface ControlSettings {
    type: ControlType,
    label?: string,
    name?: string,
    onChange?: Function | string
    options?: Array<ControlOption>
    wrap?: string
}

export interface ControlOption {
    label: string
    value: string
    wrap?: string
}

export interface CallbackMapping {
    [key: string]: Function
}


export class CheckboxControl implements ControlElement {
    $element: Cash;
    settings: ControlSettings;
    defaults = {
        type: ControlType.Checkbox,
        label: 'Checkbox control',
        name: ID(),
        onChange: (event: any) => {
            console.log('Checkbox changed', event);
        },
        wrap: '<div class="bw-control bw-control--checkbox">'
    };

    constructor(settings?: ControlSettings) {
        this.settings = $.extend({}, this.defaults, settings);
        this.createElement();
        this.bindEvents();
    }

    createElement() {

        this.$element = $(this.settings.wrap);

        const id = ID() + '-' + this.settings.name + '-';

        if (this.settings.label) {
            let $label = $('<label>').html(this.settings.label);

            if (this.settings.options.length === 1) {
                $label.attr('for', id + '0');
            }

            $label.appendTo(this.$element);
        }

        this.settings.options.forEach((option, i) => {

            $('<input type="checkbox">').attr({
                name: this.settings.name,
                id: id + i.toString(),
                value: option.value
            }).appendTo(this.$element);

            $('<label>')
                .html(option.label)
                .attr('for', id + i.toString())
                .appendTo(this.$element);
        });

    }

    bindEvents(): void {
        const self = this;
        $('input', this.$element).on('change', (event => {
            (self.settings.onChange as Function)(event);
        }));
    }


}

export class RadioControl implements ControlElement {
    settings: ControlSettings;
    $element: Cash;
    defaults = {
        type: ControlType.Radio,
        label: 'Radio control',
        onChange: (event: any) => {
            console.log('Radio changed', event);
        },
        name: ID(),
        options: [{
            label: 'Option 1',
            value: '1',
            wrap: ''
        }, {
            label: 'Option 2',
            value: '2',
            wrap: ''
        }],
        wrap: '<div class="bw-control bw-control--radio">'
    };

    constructor(settings?: ControlSettings) {
        this.settings = $.extend({}, this.defaults, settings);
        this.createElement();
        this.bindEvents();
    }

    createElement(): void {

        this.$element = $(this.settings.wrap);

        const id = ID() + '-' + this.settings.name + '-';

        // label
        if (this.settings.label) {
            $('<label>').html(this.settings.label).appendTo(this.$element);
        }

        // elements
        this.settings.options.forEach((option, i) => {


            // input element
            let $input = $('<input>')
                .attr({
                    id: id + i.toString(),
                    name: this.settings.name,
                    type: 'radio',
                    value: option.value
                });

            if (i === 0) {
                $input.attr('checked', '');
            }

            $input.appendTo(this.$element);


            // label element
            $('<label>')
                .attr('for', id + i.toString())
                .html(option.label)
                .appendTo(this.$element);
        });
    }

    bindEvents(): void {
        const self = this;
        $('input', this.$element).on('change', (event: Event) => {
            (self.settings.onChange as Function)(event);
        });
    }
}

export const ControlElements: any = {
    CheckboxControl,
    RadioControl
};
