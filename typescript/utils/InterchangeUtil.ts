import $, {Cash} from 'cash-dom';
import Bw from "../bw";

const defaults: InterchangeSettings = {
    selector: '[data-interchange]'
};

export interface InterchangeSettings {
    selector?: string
}

export class InterchangeUtil {

    public defaults: InterchangeSettings;
    public settings: InterchangeSettings;
    public elements: Cash;

    constructor(settings?: InterchangeSettings) {
        this.settings = $.extend({}, defaults, this.defaults, settings);
        this.init();
    }

    private init() {
        this.cacheDom();
        this.interchange();
        this.bindListener();
    }

    private cacheDom() {
        this.elements = $(this.settings.selector);
    }

    private interchange() {

        let newFileName = '';

        $(this.elements).each((i, e) => {

            const attr = $(e).attr('data-interchange').match(/(?!\[)([.\S]+\w)/g);

            for (let i = 0; i < attr.length / 2; i++) {
                const size = attr[(i * 2) + 1];

                if (Bw.MediaQuery.atLeast(size)) {
                    newFileName = attr[i * 2]
                }
            }

            // set new file
            if ($(e).prop('tagName') === 'IMG') {
                $(e).attr('src', newFileName);
            } else {
                $(e).css('background-image', 'url(' + newFileName + ')');
            }

        });
    }

    private bindListener() {
        $(document).on('bw.mediaQuery.changed', (e, sizes) => {
            if (sizes[0] !== sizes[1]) {
                this.interchange();
            }
        });
    }

    public update(settings: InterchangeSettings) {
        this.settings = $.extend(this.settings, this.defaults, settings);
        this.interchange();
    }
}
