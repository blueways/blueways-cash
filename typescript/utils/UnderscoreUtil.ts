// Function collection from:
// https://github.com/you-dont-need/You-Dont-Need-Lodash-Underscore
export function debounce(func: Function, wait: number, immediate?: boolean) {
    let timeout: NodeJS.Timeout;
    return () => {
        // @ts-ignore
        let context = this, args = arguments;
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        }, wait);
        if (immediate && !timeout) func.apply(context, args);
    };
}

export const isEmpty = (obj: any) => [Object, Array].includes((obj || {}).constructor) && !Object.entries((obj || {})).length;

// https://gist.github.com/gordonbrander/2230317
export const ID = function () {
    // Math.random should be unique because of its seeding algorithm.
    // Convert it to base 36 (numbers + letters), and grab the first 9 characters
    // after the decimal.
    return '_' + Math.random().toString(36).substr(2, 9);
};

declare global {
    interface String {
        toCamelCase(): String;
    }
}

String.prototype.toCamelCase = function () {
    return this.replace(/-([a-z])/g, function (g) {
        return g[1].toUpperCase();
    });
};

// { calendar-uids: "1" } => { calendarUids: "1" }
export function camelCaseObjectKeys(o: any) {
    Object.keys(o).forEach(function (key, value) {
        const newKey = key.toCamelCase();
        if (newKey !== key) {
            Object.defineProperty(o, newKey.toString(), Object.getOwnPropertyDescriptor(o, key));
            delete o[key];
        }
    });
    return o;
}

