import $ from 'cash-dom';
import _ from "underscore";
import {debounce} from "./UnderscoreUtil";


const defaults: MediaQuerySettings = {
    breakpoints: ['small', 'medium', 'large', 'xlarge', 'xxlarge'],
    breakpointWidths: [0, 640, 1024, 1200, 1440],
};

export interface MediaQuerySettings {
    breakpoints?: Array<string>,
    breakpointWidths?: Array<number>
}

export class MediaQueryUtil {

    public settings: MediaQuerySettings;
    public current: string;
    public defaults: MediaQuerySettings;

    public constructor(settings?: MediaQuerySettings) {
        this.settings = $.extend({}, defaults, this.defaults, settings);
        this.init();
    }

    public update(settings: MediaQuerySettings): void {
        this.settings = $.extend(this.settings, this.defaults, settings);
        this.updateCurrentState();
    }

    private updateCurrentState(): void {
        this.current = this.getCurrentState();
    }

    private getCurrentState(): string {
        const currentWidth = $('body').width();

        for (let i = 1; i < this.settings.breakpointWidths.length; i++) {
            if (currentWidth < this.settings.breakpointWidths[i]) {
                return this.settings.breakpoints[i - 1];
            }
        }

        // use last breakpoint
        return this.settings.breakpoints[this.settings.breakpointWidths.length - 1];
    }

    private init(): void {
        this.updateCurrentState();
        this.startWatcher();
    }

    private startWatcher(): void {

        const self = this;

        $(window).on('resize', debounce(() => {

            const oldSize = this.current;
            const newSize = this.getCurrentState();

            if (oldSize !== newSize) {
                this.updateCurrentState();
                $(document).trigger('bw.mediaQuery.changed', [newSize, oldSize]);
            }

        }, 100));
    }

    public atLeast(size: string): boolean {
        if (this.settings.breakpoints.indexOf(size) === -1) {
            console.error('Breakpoint name not found in settings');
            return false;
        }

        const atLeastBreakpointIndex = this.settings.breakpoints.indexOf(size);
        const currentBreakpointIndex = this.settings.breakpoints.indexOf(this.current);

        return atLeastBreakpointIndex <= currentBreakpointIndex;
    }
}
