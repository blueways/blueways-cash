# Cash plugins

This repository contains JavaScript modules and helper based on [cash](https://github.com/kenwheeler/cash) (jQuery alternative).

## Development

Source files are written in SASS and TypeScript. The build is done with gulp

### Build

```
$ gulp build
```

### Preview server

All modules can be tested inside a the preview files `/src/index.html` and `src/js/index.js`. There is a [webpack](https://webpack.js.org) server configured for easy development and preview. To start the server, run `npm start`.

## Plugins

* Media Query
* Interchange
* Menu Aim
* Mobile navigation
* Validator
* Desktop navigation
* Lightbox
* Wizardbox

## Media Query

Inspired by [Foundation Media Queries component](https://get.foundation/sites/docs/media-queries.html).

**Requirements**: custom.underscore.js

The plugin is initialized automatically. 

#### Triggered Events

Event name|Parameter|Description
--- | --- | ---
changed.bw.mediaquery|[newSize, oldSize]|Triggerd on window resize

#### Examples 

```javascript
// listening to breakpoint changes:
$(document).on('changed.bw.mediaquery', function(oldSize, newSize){
    console.log('Breakpoint changed from ' + oldSize + ' to ' + newSize);
});

// get current breakpoint
Bw.MediaQuery.current; // e.g. returns 'small' 

// check if breakpoint size is fulfilled
Bw.MediaQuery.atLeast('large'); // e.g. returns false

// check for current breakpoint name 
Bw.MediaQuery.is('small'); // e.g. returns true

// override default settings
const options = {
    breakpoints: ['small', 'medium', 'large', 'xlarge', 'xxlarge'],
    breakpointWidths: [0, 640, 1024, 1200, 1440],
};
Bw.MediaQuery = new MediaQuery(options);
```

## Interchange

Inspired by [Foundation Interchange plugin](https://get.foundation/sites/docs/interchange.html).

**Requirements**: cash.media-query.js 

Init plugin with:

```javascript
$.Interchange(options);
```

Options may contain:

```javascript
const options = {
    selector: '[data-interchange]' // Selector for elements to observe 
};
```

## Menu Aim

Cash implementation of [jQuery-menu-aim](https://github.com/kamens/jQuery-menu-aim).

## Mobile navigation

Example pages: [Studentenwerk Halle](https://www.studentenwerk-halle.de/), [SHK Dresden](https://shk-dresden.de/)

**Required**: cash.media-query.js

Init plugin with:

```javascript
$.MobileNav(options);
```

Options may contain:

```javascript
const options = {
    showFor: ['small'],
    selectorHeader: 'header.topbar2',
    selectorMenuButton: '.menu-button',
    selectorMenuLinks: '.menu.level1 li a',
    isOpenClass: 'is-open' 
};
```

## Validator

Validate form inputs and prevent form submission. Example Markup:

**HTML**
````html
<form data-validate="true">
    
    <!-- Text input -->
    <label for="text-input">Text</label>
    <input required id="text-input" type="text" name="text" value="" />
    
    <!-- Email input -->
    <label for="email-input">Email</label>
    <input required id="email-input" type="email" name="email" value="" />
    
    <!-- Select input -->
    <label for="select-input">Selection</label>
    <select data-validator="option_selected" name="select" id="select-input" required>
        <option>Please choose</option>
        <option value="1">Option 1</option>
        <option value="2">Option 2</option>
    </select>
    
    <!-- Checkbox input -->
    <label for="checkbox-input">Checkbox</label>
    <input type="checkbox" data-validator="is_checked" id="checkbox-input" />

    <button type="submit">Submit</button> 
</form>
````

**JavaScript**
```javascript
$.myFormValidator = $.Validator({
    selector: {
        submitButton: 'form[data-validate="true"] button[type="submit"]'
    }
});

// manually check the form for validation errors 
$.myFormValidator.isValidForm(); // return true or false
```

## Desktop navigation

## Lightbox

Simple image and content lightbox.

**Requirements**: cash.lightbox.css

#### Initialization:

**HTML**
```html
<a href="large-image1.jpg" class="lightbox" rel="gallery1">image1.jpg</a>
<a href="large-image2.jpg" class="lightbox" rel="gallery1">image2.jpg</a>
```

**JavaScript**
```javascript
$.box = $.Lightbox(options);

// display html content
$.box.displayContent('<h1>Hello world!</h1>');

```

#### Default options
```javascript
const options = {
    selector: 'a.lightbox',
    groupSelector: 'rel',
    lightboxSelector: '.bw-lightbox',
    markup: '...'
};
```

## Wizardbox

![Wizardbox clip](example/index_files/wizard.gif)

Lightbox that can display HTML content and navigate between different steps.

**Requirements**: cash.wizardbox.css

#### Initialization

```javascript
const myWizard = $.Wizardbox({
    steps: [
        {
            id: 0,
            content: '<h1>Hello World!</h1>',
            title: 'First'
        },
        {
            id: 1,
            content: '<h1>Hello from Step 2</h1>',
            title: 'Second'
        },
        {
            id: 'success',
            content: 'Success!',
            size: 'small'
        }
    ],
    isCloseable: false,
    showNavigator: false,
    size: 'large'
});
``` 

### Options

Parameter|Type|Default|Description
--- | --- | --- | ---
steps | Array | - | Array of step objects
isCloseable | Boolean | true | Display close button and enable closing of wizard
showNavigator | Boolean | true | Display navigation
dynamicHeight | Boolean | false | Change the height between slide changes
fadeIn | Boolean | true | Adds a fading animation to the show up
size | String | medium | Default size of the lightbox
icons | object | { check: '..' } | Custom icons can be added here

### Step object

Key|Type|Required|Description
--- | --- | --- | ---
content | String/Object | yes | HTML markup of step content
id | String / Integer | yes | Unique identifier
size | String | no | Size of the step overrides default lightbox setting
title | String | no | Title for navigator
icon | String | no | Id of the icon, defaults to the number of offset

### Navigation
You can link between slides via data attributes:
```html
<a href="#" data-to-step="2">Animate to step with key 2</a>
<a href="#" data-to-step="success" data-animate="false">Show success slide without animation</a>
<a href="#" data-close>Close Wizard</a>
```

### Events

Event name|Description|Parameter
--- | --- | ---
bw.wizardbox.beforeInit|Fired before anything is done|-
bw.wizardbox.beforeCacheDom|Fired after markup was insert|-
bw.wizardbox.beforeSetup|Fired after DOM was cached|-
bw.wizardbox.beforeBindEvent| Fired after settings have been set|-
bw.wizardbox.afterInit|Fired at end of initialization|-
bw.wizardbox.closed|Fired when wizard gets closed|-
bw.wizardbox.beforeChangeStep|Fired before changing to a new step|stepName, animate, markSuccess
bw.wizardbox.afterChangeStep|Fired after changed to a new step|stepName, animate, markSuccess

### Methods

Public functions callable

#### open
Opens the wizard lightbox
```javascript
$.myWizard.open(0);
```

Parameter|Type|Default|Description
--- | --- | --- | ---
stepName | String or Integer | - | Name of the step the wizard should start wit

#### close
Close the window
```javascript
$.myWizard.close();
```

#### toStep
Navigate to the given slide
```javascript
$.myWizard.toStep('success', false, false);
```

Parameter|Type|Default|Description
--- | --- | --- | ---
stepId | String or Integer | - | Name of a step
animate | Boolean | true | Slide change can be animated (500ms)
markSuccess | Boolean | true | Mark current navigation item as successful

#### addStep
Add a new slide to the wizard
```javascript
$.myWizard.addStep({id: 'new', content: 'New content', title: 'New step', size: 'medium', 'icon': 'custom'}, 3);
```

Parameter|Type|Default|Description
--- | --- | --- | ---
step | Object<Step> | - | Step object
position | int | - | Position of the new step (default at end)

#### makeClosable
Displays the close button and activates the "ESC" key + click outside of the wizard to close the window.
```javascript
$.myWizard.makeClosable();
```

#### stopClosable
Removes the possibility of closing the wizard
```javascript
$.myWizard.stopClosable();
```

#### startLoader
Starts loading animation
```javascript
$.myWizard.startLoader(false);
```
Parameter|Type|Default|Description
--- | --- | --- | ---
scrollToTop | Boolean | true | Jumps to the top of the lightbox

#### stopLoader
Stops loading animation
```javascript
$.myWizard.stopLoader(false);
```

Parameter|Type|Default|Description
--- | --- | --- | ---
animate | Boolean | true | Adds some delay to wait for step animation

#### showNavigator
Shows the step navigator
```javascript
$.myWizard.showNavigator();
```

#### hideNavigator
Hides the step navigator
```javascript
$.myWizard.hideNavigator();
```

#### setWidth
Adjust the width of the Lightbox
```javascript
$.myWizard.setWidth('medium');
```

Parameter|Type|Default|Description
--- | --- | --- | ---
size | String | - | Change the size of the lightbox (small, medium or large)
